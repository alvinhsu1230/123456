 truncate T_somaterial;

FOR r IN (
  -- select * from T_somaterial  
  -- drop table T_somaterial
  -- create table T_somaterial as 
   select DISTINCT  0 ad_pinstance_id 
  -- ID to Value --

  ,ba.M_ProductBOM_ID
  ,(select Value from M_Product where M_Product_id = ba.M_ProductBOM_ID) product_value
  ,pd.Name as product_Name
  ,(select Description from M_Product where M_Product_id = ba.M_ProductBOM_ID) product_Description
  ,ba.Description as plaement 
  ,(select DISTINCT Value from M_AttributeInstance where M_Attribute_ID in(select M_Attribute_ID from M_AttributeValueCategory where AttributeValueCategory ='Color') and 
  	M_AttributeSetInstance_ID =ba.M_AttributeSetInstance_ID
   ) color 
   ,(select M_Product_Category_Parent_ID from M_Product_Category where M_Product_Category_ID=pd.M_Product_Category_ID) M_Product_Category
   from BOM_Attribute ba inner join M_Product pd on ba.M_ProductBOM_ID = pd.M_Product_ID 
   where ba.M_Product_ID in(select M_ProductBOM_ID from M_Product_BOM pb 
					  where M_Product_ID in(select ol.M_Product_ID from C_orderline ol left join M_product pd on ol.M_product_id=pd.M_product_id
											where pd.IsBOM='Y' and ol.C_order_id in(select C_order_id from C_order 
  							                                    where C_project_id=p_Record_ID and issotrx ='Y')))
   or ba.M_Product_ID in(select ol.M_Product_ID from C_orderline ol left join M_product pd on ol.M_product_id=pd.M_product_id
											where pd.IsBOM='Y' and ol.C_order_id in(select C_order_id from C_order 
  							                                    where C_project_id=p_Record_ID and issotrx ='Y')) 																
