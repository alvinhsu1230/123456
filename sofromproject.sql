-- FUNCTION: adempiere.so_sofromproject_rpt(numeric)

-- DROP FUNCTION adempiere.so_sofromproject_rpt(numeric);

CREATE OR REPLACE FUNCTION adempiere.so_sofromproject_rpt(
	pinstance numeric)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
DECLARE
/**
p--- ERP 帶入的參數
v--- procedure 運算用的變數 
*/
ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql VARCHAR (2000);
sqldel VARCHAR (2000);

p RECORD;
r RECORD;
r1 RECORD;

--p_C_Project_ID NUMERIC(10):=0;

p_DateAcct DATE;
p_DateAcct_TO DATE;

p_Record_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_User_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
p_AD_Org_ID_TO NUMERIC(10) := 0;
p_IsSOTrx VARCHAR (1) := 'Y';
p_C_DocType_ID NUMERIC(10) := 0;
p_C_DocType_ID_2 NUMERIC(10) := 0;

p_C_BPartner_Value VARCHAR (40) := 0;
p_C_DocType_Value VARCHAR (40) := 0;
p_C_BPartner_Value_TO VARCHAR (40) := '';
p_C_DocType_Value_TO VARCHAR (40) := '';

p_MovementDate DATE;
p_MovementDate_TO DATE;
p_DateInvoiced DATE;
p_DateInvoiced_TO DATE;

v_message VARCHAR (400) := '';
v_documentno VARCHAR (30) := '';
v_count NUMERIC(10) := 0;
v_NextNo NUMERIC(10) := 0;
v_AD_Workflow_ID NUMERIC(10) := 0;

BEGIN

IF pinstance is null THEN 
	pinstance:=0;
END IF;

v_message :='程式開始:: 更新[呼叫程序]紀錄檔...開始執行時間(created)..程序執行中(isprocessing)..';
--IF pinstance > 0 THEN
BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE adempiere.ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='Parameter loading';

FOR p IN
(SELECT pi.record_id, pi.ad_client_id,pi.ad_org_id, pi.ad_user_id, 
pp.parametername,
pp.p_string, pp.p_number, pp.p_date,
pp.p_string_TO, pp.p_number_TO, pp.p_date_tO
FROM adempiere.ad_pinstance pi
LEFT OUTER JOIN adempiere.ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
WHERE pi.ad_pinstance_id = pinstance
ORDER BY pp.SeqNo)
LOOP
p_Record_ID := p.record_id;
p_User_ID := p.AD_User_id;
p_AD_Client_ID := p.AD_Client_ID;
p_AD_Org_ID := p.AD_Org_ID;
--p_C_Project_ID := p.C_Project_ID;

  IF p.parametername = 'DateAcct' THEN p_DateAcct = p.p_date;
     p_DateAcct_TO = p.p_date_to;
        
  ELSIF p.parametername = 'AD_Org_ID' THEN p_AD_Org_ID = p.p_number;
     p_AD_Org_ID_TO = p.p_number_to;
  ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;
  --ELSIF p.parametername = 'C_Project_ID' THEN p_C_Project_ID = p.p_number;
  END IF;

END LOOP;

IF p_User_ID IS NULL THEN 
   p_User_ID := 0; 
END IF;

-- 測試用
IF pinstance = 1000000 THEN

p_Record_ID = 1000002;

END IF;

v_message :='Start Process';

TRUNCATE T_sofromproject;

/* 保留字需要雙引號  */

/* 測試 CODE
select  so_sofromproject_rpt(1000000)
select * from T_sofromproject
select  ad_pinstance_id, errormsg from  adempiere.ad_pinstance where ad_pinstance_id = 1000000
*/
v_message :='FOR r IN (';
FOR r IN (
  -- select * from T_sofromproject
  -- drop table T_sofromproject
  -- create table T_sofromproject as 
  select 0 ad_pinstance_id 
  -- ID to Value --
	
  ,pj.c_project_id
  ,pj.forcast
  ,pj.Value as ProjectValue
  ,pj.name as ProjectName
  ,pj.DateContract
  ,(select MAX(updated) from C_order where c_project_id=pj.c_project_id)as updated
  ,(select Name from AD_Org where AD_Org.AD_Org_id = pj.AD_Org_id) org_name
  ,(select name from C_BPartner where C_BPartner_id = pj.C_BPartner_id) BPartner_Name
  ,(select name from Season where  Season_ID = pj.Season_ID) Season_Name
  ,(select name from AD_User where  AD_User_ID = pj.AD_User_ID) AD_User_Name
  ,(select name from AD_User where  AD_User_ID = pj.IVG_FollowUp_User_ID) IVG_FollowUp_Name
  ,(select name from AD_User where  AD_User_ID = pj.CVG_FollowUp_User_ID) CVG_FollowUp_Name
  ,(select name from AD_User where AD_User_id = pj.SalesRep_id) SalesRep_Name
  ,(select iso_code from C_Currency where C_Currency_ID = pj.C_Currency_ID) Currency_Name
  ,pj.Description
  ,pj.Factory
  ,pj.DateFinish
  ,(select Value from M_Product where M_Product_id in(select M_Product_id from c_projectline where c_project_id=pj.c_project_id)) Style_No
  ,(select SUM(QtyEntered)from C_order o inner join C_orderline ol on o.C_order_id=ol.C_order_id
      where o.c_project_id=pj.c_project_id and issotrx ='Y' group by o.c_project_id) Total_QTY
  ,(select SUM(GrandTotal) from C_order where c_project_id=pj.c_project_id and issotrx ='Y') GrandTotal 
	,(select binarydata from AD_image where AD_image_id=pj.AD_image_id)as img
  from C_Project pj
	where pj.c_project_id=p_Record_ID
	
)LOOP
v_message :='LOOP process';

INSERT INTO T_sofromproject(
	        ad_pinstance_id,c_project_id,forcast,ProjectValue,ProjectName,DateContract,updated,org_name,BPartner_Name,Season_Name
	        ,AD_User_Name,IVG_FollowUp_Name,CVG_FollowUp_Name,SalesRep_Name,Currency_Name,Description,Factory,DateFinish
	        ,Style_No,Total_QTY,GrandTotal,img
            )
    VALUES (
           	pinstance,r.c_project_id,r.forcast,r.ProjectValue,r.ProjectName,r.DateContract,r.updated,r.org_name,r.BPartner_Name,r.Season_Name
	        ,r.AD_User_Name,r.IVG_FollowUp_Name,r.CVG_FollowUp_Name,r.SalesRep_Name,r.Currency_Name,r.Description,r.Factory,r.DateFinish
	        ,r.Style_No,r.Total_QTY,r.GrandTotal,r.img
            );
END LOOP;
v_message :='r1';
FOR r1 IN (
  -- select * from T_soquantity
  -- drop table T_soquantity
  -- create table T_soquantity as 
  select 0 ad_pinstance_id 
  -- ID to Value --
	
 ,ol.POReference,ol.QtyEntered,ol.Actual_CRD,o.c_project_id,ol.M_AttributeSetInstance_ID
 ,(select name from PO_Type where PO_Type_ID= ol.PO_Type_ID) po_type_name
 ,(select ShipperAccount from C_BP_ShippingAcct where C_BP_ShippingAcct_ID= ol.C_BP_ShippingAcct_ID) BPartnerShippingAcctName
 ,(select Name from M_Product where M_Product_id = ol.M_Product_ID) Style_No
 ,(select name from C_BPartner_Location where C_BPartner_Location_ID= ol.C_BPartner_Location_ID) LocationName

 ,(select Value from M_AttributeInstance where M_Attribute_ID in(select M_Attribute_ID from M_AttributeValueCategory where AttributeValueCategory ='Size') 
 and M_AttributeSetInstance_ID =ol.M_AttributeSetInstance_ID)as Attributesize
	
 ,(select Sequence from M_AttributeValue where M_Attribute_ID in
	  (select M_Attribute_ID from M_AttributeValueCategory where AttributeValueCategory ='Size') 
	  and M_AttributeValue_ID in 
	  (select M_AttributeValue_ID from M_AttributeInstance where M_AttributeSetInstance_ID in
	   (select M_AttributeSetInstance_ID from M_AttributeSetInstance where M_AttributeSetInstance_ID=ol.M_AttributeSetInstance_ID))
  ) as AttributesizeSequence	
 , get_proportion(ol.c_orderline_id) proportionRate
 from C_orderline ol inner join C_order o on ol.C_order_id=o.C_order_id 
	where 
	o.issotrx ='Y' 
	and ol.POReference is not null 
	and o.c_project_id=p_C_Project_ID
 group by o.c_project_id,ol.POReference,Style_No,ol.QtyEntered,Attributesize,Actual_CRD,po_type_name,BPartnerShippingAcctName,LocationName,proportionRate
	,ol.M_AttributeSetInstance_ID
	order by ol.POReference,AttributesizeSequence
 
  
)LOOP
v_message :='LOOP process';

INSERT INTO T_soquantity(
	         ad_pinstance_id,POReference,QtyEntered,Actual_CRD,po_type_name,BPartnerShippingAcctName,Style_No,LocationName,
	        Attributesize,proportionRate
            )
    VALUES (
           	pinstance,r1.POReference,r1.QtyEntered,r1.Actual_CRD,r1.po_type_name,r1.BPartnerShippingAcctName,r1.Style_No,r1.LocationName,
	        r1.Attributesize,r1.proportionRate
            );

END LOOP;

v_message :='END Process';

IF pinstance > 0 THEN
BEGIN
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;

EXCEPTION

WHEN OTHERS THEN
--sqlins := 'INSERT INTO adempiere.t_an_shipment (ad_pinstance_id, t_an_shipment_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 9, v_message;
--v_message :='例外錯誤。。。';
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END;
$BODY$;

ALTER FUNCTION adempiere.so_sofromproject_rpt(numeric)
    OWNER TO adempiere;
