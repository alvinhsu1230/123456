FOR r1 IN (
  -- select * from T_soquantity
  -- drop table T_soquantity
  -- create table T_soquantity as 
  select 0 ad_pinstance_id 
  -- ID to Value --
	
 ,ol.POReference,ol.QtyEntered,ol.Actual_CRD,o.c_project_id,ol.M_AttributeSetInstance_ID
 ,(select name from PO_Type where PO_Type_ID= ol.PO_Type_ID) po_type_name
 ,(select ShipperAccount from C_BP_ShippingAcct where C_BP_ShippingAcct_ID= ol.C_BP_ShippingAcct_ID) BPartnerShippingAcctName
 ,(select Name from M_Product where M_Product_id = ol.M_Product_ID) Style_No
 ,(select name from C_BPartner_Location where C_BPartner_Location_ID= ol.C_BPartner_Location_ID) LocationName

 ,(select Value from M_AttributeInstance where M_Attribute_ID in(select M_Attribute_ID from M_AttributeValueCategory where AttributeValueCategory ='Size') 
 and M_AttributeSetInstance_ID =ol.M_AttributeSetInstance_ID)as Attributesize
	
 ,(select Sequence from M_AttributeValue where M_Attribute_ID in
	  (select M_Attribute_ID from M_AttributeValueCategory where AttributeValueCategory ='Size') 
	  and M_AttributeValue_ID in 
	  (select M_AttributeValue_ID from M_AttributeInstance where M_AttributeSetInstance_ID in
	   (select M_AttributeSetInstance_ID from M_AttributeSetInstance where M_AttributeSetInstance_ID=ol.M_AttributeSetInstance_ID))
  ) as AttributesizeSequence	
 , get_proportion(ol.c_orderline_id) proportionRate
 from C_orderline ol inner join C_order o on ol.C_order_id=o.C_order_id 
	where 1=1
	and o.issotrx ='Y' 
	and ol.POReference is not null 
	--and o.c_project_id=p_Record_ID
 group by o.c_project_id,ol.POReference,Style_No,ol.QtyEntered,Attributesize,Actual_CRD,po_type_name,BPartnerShippingAcctName,LocationName,proportionRate
	,ol.M_AttributeSetInstance_ID
	order by ol.POReference,AttributesizeSequence
 
  
